<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Transaction extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $EmailAddress = $this->get('emailaddress');
        if ($EmailAddress == '') {
            $login = $this->db->get('tbltransaction')->result();
        } else {
            $this->db->where('emailaddress', $EmailAddress);
            $login = $this->db->get('tbltransaction')->result();
        }
        $this->response($login, 200);
    }

	//Mengirim atau menambah data kontak baru
	function index_post() {
        $data = array(
                    'transactiontype'			=> $this->post('transactiontype')
					,'emailaddress'      => $this->post('emailaddress')
					,'emailaddresssender'      => $this->post('emailaddresssender')
					,'emailaddressrecipient'      => $this->post('emailaddressrecipient')
					,'transactiondate'      => $this->post('transactiondate')
					,'notes'      => $this->post('notes')
					,'currencyid'      => $this->post('currencyid')
					,'amount'      => $this->post('amount')
					,'currentbalance'      => $this->post('currentbalance')
					,'accountstatementid'      => $this->post('accountstatementid')
					,'lastaccountstatementid'      => $this->post('lastaccountstatementid')
					);
        $insert = $this->db->insert('tbltransaction', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Memperbarui data transaction yang telah ada
	function index_put() {
        $id = $this->put('id');
        $data = array(
					'transactiontype'			=> $this->put('transactiontype')
					,'emailaddress'      => $this->post('emailaddress')
					,'emailaddresssender'      => $this->post('emailaddresssender')
					,'emailaddressrecipient'      => $this->post('emailaddressrecipient')
					,'transactiondate'      => $this->post('transactiondate')
					,'notes'      => $this->post('notes')
					,'currencyid'      => $this->post('currencyid')
					,'amount'      => $this->post('amount')
					,'currentbalance'      => $this->post('currentbalance')
					,'accountstatementid'      => $this->post('accountstatementid')
					,'lastaccountstatementid'      => $this->post('lastaccountstatementid')
					);
        $this->db->where('id', $id);
        $update = $this->db->update('tbltransaction', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Menghapus salah satu data kontak
	function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('id');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}