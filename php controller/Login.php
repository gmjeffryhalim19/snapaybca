<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Login extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $EmailAddress = $this->get('emailaddress');
        if ($EmailAddress == '') {
            $login = $this->db->get('telepon')->result();
        } else {
            $this->db->where('emailaddress', $EmailAddress);
            $login = $this->db->get('telepon')->result();
        }
        $this->response($login, 200);
    }

	//Mengirim atau menambah data kontak baru
	function index_post() {
        $data = array(
                    'nama'      => $this->post('nama'),
					'nomor'    => $this->post('nomor'),
					'dateofbirth'    => $this->post('dateofbirth'),
					'customername'    => $this->post('customername'),
					'emailaddress'    => $this->post('emailaddress'),
					'companycode'    => $this->post('companycode'),
					'customernumber'    => $this->post('customernumber'),
					'password'		=> $this->post('password'),
					'ktpno'			=> $this->post('ktpno'),
					'accountno'			=> $this->post('accountno'));
        $insert = $this->db->insert('telepon', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Memperbarui data kontak yang telah ada
	function index_put() {
        $EmailAddress = $this->put('EmailAddress');
        $data = array(
                    'id'           => $this->put('id'),
                    'nama'          => $this->put('nama'),
                    'nomor'    => $this->put('nomor'),
					'nomor'    => $this->put('nomor'),
					'dateofbirth'    => $this->put('dateofbirth'),
					'customername'    => $this->put('customername'),
					'emailaddress'    => $this->put('emailaddress'),
					'companycode'    => $this->put('companycode'),
					'customernumber'    => $this->put('customernumber'),
					'password'		=> $this->put('password'),
					'ktpno'		=> $this->put('ktpno'),
					'accountno'		=> $this->put('accountno'));
        $this->db->where('emailaddress', $EmailAddress);
        $update = $this->db->update('telepon', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Menghapus salah satu data kontak
	function index_delete() {
        $id = $this->delete('id');
        $this->db->where('emailaddress', $EmailAddress);
        $delete = $this->db->delete('telepon');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}