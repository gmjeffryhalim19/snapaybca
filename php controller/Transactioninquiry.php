<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Transactioninquiry extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $login = $this->db->get('tbltransaction')->result();
        } else {
            $this->db->where('id', $id);
            $login = $this->db->get('tbltransaction')->result();
        }
        $this->response($login, 200);
    }

	//Mengirim atau menambah data kontak baru
	function index_post() {
        $data = array(
                    'transactiontype'			=> $this->post('transactiontype')
					,'emailaddress'      => $this->post('emailaddress')
					,'emailaddresssender'      => $this->post('emailaddresssender')
					,'emailaddressrecipient'      => $this->post('emailaddressrecipient')
					,'requestfrom'      => $this->post('requestfrom')
					,'transactiondate'      => $this->post('transactiondate')
					,'notes'      => $this->post('notes')
					,'currencyid'      => $this->post('currencyid')
					,'requestamount'      => $this->post('requestamount')
					,'amount'      => $this->post('amount')
					,'statuspayment'      => $this->post('statuspayment')
					,'paymentpicture'      => $this->post('paymentpicture')
					,'currentbalance'      => $this->post('currentbalance')
					,'accountstatementid'      => $this->post('accountstatementid')
					,'lastaccountstatementid'      => $this->post('lastaccountstatementid')
					,'transactionid'      => $this->post('transactionid')
					,'ischannel'      => $this->post('ischannel')
					);
        $insert = $this->db->insert('tbltransaction', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Memperbarui data transaction yang telah ada
	function index_put() {
        $id = $this->put('id');
        $data = array(
					'transactiontype'			=> $this->put('transactiontype')
					,'emailaddress'      => $this->put('emailaddress')
					,'emailaddresssender'      => $this->put('emailaddresssender')
					,'emailaddressrecipient'      => $this->put('emailaddressrecipient')
					,'requestfrom'      => $this->put('requestfrom')
					,'transactiondate'      => $this->put('transactiondate')
					,'notes'      => $this->put('notes')
					,'currencyid'      => $this->put('currencyid')
					,'requestamount'      => $this->put('requestamount')
					,'amount'      => $this->put('amount')
					,'statuspayment'      => $this->put('statuspayment')
					,'paymentpicture'      => $this->put('paymentpicture')
					,'currentbalance'      => $this->put('currentbalance')
					,'accountstatementid'      => $this->put('accountstatementid')
					,'lastaccountstatementid'      => $this->put('lastaccountstatementid')
					,'transactionid'      => $this->put('transactionid')
					,'ischannel'      => $this->put('ischannel')
					);
        $this->db->where('id', $id);
        $update = $this->db->update('tbltransaction', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Menghapus salah satu data kontak
	function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('id');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}