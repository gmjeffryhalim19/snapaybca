<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Friend2 extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $EmailAddress = $this->get('emailaddress');
		$EmailAddressFriend = $this->get('emailaddressfriend');
        //if ($EmailAddress == '') {
        //    $login = $this->db->get('tblfriendlist')->result();
        //} else {
			
			//$sql = "SELECT * FROM tblfriendlist WHERE emailaddress = ? OR emailaddressfriend = ?";
            //$sql = "SELECT * FROM tblfriendlist WHERE emailaddress = 'a'";
			//$this->db->where('emailaddress','', $EmailAddress);
			//$login = $this->db->query($sql, array(2, $EmailAddress , $EmailAddressFriend));
            //$login = $this->db->get('tblfriendlist')->result();
			//$login = $this->db->result();
			$login = $this->db->query("SELECT * FROM tblfriendlist WHERE emailaddress = 'a' OR emailaddressfriend='a'");
        //}
		$this->response($login, 200);
    }

	//Mengirim atau menambah data kontak baru
	function index_post() {
        $data = array(
                    'emailaddress'			=> $this->post('emailaddress')
					,'emailaddressfriend'      => $this->post('emailaddressfriend'));
        $insert = $this->db->insert('tblfriendlist', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Memperbarui data kontak yang telah ada
	function index_put() {
        $EmailAddress = $this->put('EmailAddress');
        $data = array(
                    'emailaddressfriend'           => $this->put('emailaddressfriend'));
        $this->db->where('emailaddress', $EmailAddress);
        $update = $this->db->update('tblfriendlist', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
	
    //Menghapus salah satu data kontak
	function index_delete() {
        $id = $this->delete('id');
        $this->db->where('emailaddress', $EmailAddress);
        $delete = $this->db->delete('emailaddress');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}