package com.example.jeffry.snapaybca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BalanceActivity extends AppCompatActivity {

    static String CustomerName,UserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        String EmailAddress;

        //Ambil data dari Activity sebelumnya
        Intent intent = getIntent();

        CustomerName = intent.getStringExtra("customername");
        UserName = intent.getStringExtra("username");

        getUserInformation();

        Button Cashout = (Button) findViewById(R.id.btnCashOut);
        Cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CashOutBalance();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void CashOutBalance(){

    }


    private void getUserInformation() {

        //Set Foto dan NamaProfile
        TextView TVBalanceUsername = (TextView) findViewById(R.id.TVBalanceUsername);
        TVBalanceUsername.setText(CustomerName);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<Nama>> call = api.getLogin();

        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                //Creating an String array for the ListView
                String[] Email = new String[CustomerList.size()];
                Double[] xBalanceInfo = new Double[CustomerList.size()];
                String[] xAccountNo = new String[CustomerList.size()];

                TextView TVAccountNo = (TextView) findViewById(R.id.TVBalanceAccountNo);
                TextView TVBalanceInformation = (TextView) findViewById(R.id.TVBalanceInformation);
                DecimalFormat formatter = new DecimalFormat("#,###");

                //looping through all the heroes and inserting the names inside the string array
                for (int i = 0; i < CustomerList.size(); i++) {
                    Email[i] = CustomerList.get(i).getemailaddress();
                    xBalanceInfo[i] = CustomerList.get(i).getbalanceinfo();
                    xAccountNo[i] = CustomerList.get(i).getaccountno();
                    if (Email[i].equals(UserName)) {
                        String get_value = formatter.format(xBalanceInfo[i]);
                        TVBalanceInformation.setText(get_value);
                        //Toast.makeText(getApplicationContext(), xAccountNo[i], Toast.LENGTH_LONG).show();
                        TVAccountNo.setText(xAccountNo[i]);
                        //Toast.makeText(getApplicationContext(), CustomerName, Toast.LENGTH_LONG).show();
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
