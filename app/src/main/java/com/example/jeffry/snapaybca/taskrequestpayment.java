package com.example.jeffry.snapaybca;

import android.content.Intent;
//import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import android.graphics.Bitmap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class taskrequestpayment extends AppCompatActivity {

    ImageView IVTRPPhoto;
    static int TP_ID;
    static String TP_Username , TP_TransactionType, TP_EmailAddress, TP_EmailAddressTo
            , TP_RequestFrom, TP_EmailAddressSender, TP_EmailAddressRecipient
            , TP_TransactionDate, TP_Notes, TP_CurrencyID, TP_PaymentPicture
            , TP_Status, TP_IsChannel = "";
    static String TP_AccountStatementID, TP_LastAccountStatementID, TP_StatusPayment, TP_TransactionID;
    static Double TP_RequestAmount, TP_Amount, TP_CurentBalance, Temp_Amount;

    static Boolean SuccessPassword;

    private Double TempAmount;

    EditText ETPayToUsername, ETAmount, ETNotes;
    Button btnTPPay, btnTPReject;

    private static final int CAMERA_REQUEST = 1888;
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taskrequestpayment);


        TP_StatusPayment="";
        Temp_Amount = 0.0;



        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        Bundle bundle = getIntent().getExtras();
        TP_ID = Integer.parseInt(bundle.getString("id"));
        TP_Username = bundle.getString("username");
        TP_TransactionType  = bundle.getString ("description");
        TP_EmailAddressRecipient = bundle.getString("requestfrom");
        TP_Status = bundle.getString("status");


        EditText ETPayToUsername = (EditText) findViewById(R.id.etTo);
        EditText ETAmount = (EditText) findViewById(R.id.etAmount);
        EditText ETNotes = (EditText) findViewById(R.id.etNotes);
        Button btnTPReject = (Button) findViewById(R.id.btnTPReject);
        Button btnTPPay = (Button) findViewById(R.id.btnTPPay);


        ETPayToUsername.setInputType(InputType.TYPE_NULL);
        ETPayToUsername.setKeyListener(null);

        if (TP_Status.equals("new")){
            //Jika Baru, maka set saja payto username

            btnTPReject.setVisibility(View.GONE);
            ETPayToUsername.setText(TP_EmailAddressRecipient);

        } else if (TP_Status.equals("edit")) {
            // Jika edit, maka load semua data
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            Api api = retrofit.create(Api.class);

            Call<List<transactioninquiry>> call = api.getTransactioninquiry2(TP_ID);

            call.enqueue(new Callback<List<transactioninquiry>>()  {
                @Override
                public void onResponse(Call<List<transactioninquiry>> call, Response<List<transactioninquiry>> response) {
                    List<transactioninquiry> TransactionList = response.body();


                    // Jika memang ada hasil inquiry maka load baris pertama saja
                    if (TransactionList.size() > 0 ) {
                        String TransactionType,EmailAddress,EmailAddressSender, EmailAddressRecipient,RequestFrom
                                ,TransactionDate,Notes,CurrencyID, PaymentPicture, StatusPayment = "";
                        Double RequestAmount,Amount = 0.0;

                        int i = 0;
                        TransactionType = TransactionList.get(i).getTransactiontype();
                        EmailAddress = TransactionList.get(i).getEmailaddress();
                        EmailAddressSender = TransactionList.get(i).getEmailaddresssender();
                        EmailAddressRecipient = TransactionList.get(i).getEmailaddressrecipient();
                        RequestFrom = TransactionList.get(i).getRequestfrom();
                        TransactionDate = TransactionList.get(i).getTransactiondate();
                        Notes = TransactionList.get(i).getNotes();
                        CurrencyID = TransactionList.get(i).getCurrencyid();
                        RequestAmount = TransactionList.get(i).getRequestamount();
                        Amount = TransactionList.get(i).getAmount();
                        PaymentPicture = TransactionList.get(i).getPaymentpicture();
                        StatusPayment = TransactionList.get(i).getStatuspayment();

                        EditText ETPayToUsername = (EditText) findViewById(R.id.etTo);
                        EditText ETAmount = (EditText) findViewById(R.id.etAmount);
                        EditText ETNotes = (EditText) findViewById(R.id.etNotes);
                        Button btnTPReject = (Button) findViewById(R.id.btnTPReject);
                        Button btnTPPay = (Button) findViewById(R.id.btnTPPay);

                        ETPayToUsername.setText(EmailAddressRecipient);
                        ETAmount.setText(Amount.toString());
                        ETNotes.setText(Notes);

                        //Load Gambar

                        //Jika status sudah complete maka ga bisa edit lagi
                        if(StatusPayment.equals("paid")){
                            ETAmount.setInputType(InputType.TYPE_NULL);
                            ETAmount.setKeyListener(null);

                            ETNotes.setInputType(InputType.TYPE_NULL);
                            ETNotes.setKeyListener(null);

                            btnTPPay.setVisibility(View.GONE);
                            btnTPReject.setVisibility(View.GONE);

                        } else {

                            //Log.e("contacttasklist", "TP Type " + TP_TransactionType + " TP RF" + TP_RequestFrom);
                            //Cek mau load form apa (Payment atau Request Payment)
                            if (TP_TransactionType.equals("payment") && RequestFrom.equals("")) {
                                //Murni Tipe Payment saja (Tanpa Request dari Orang Lain)

                            } else if (TP_TransactionType.equals("Payment") && RequestFrom.equals(TP_EmailAddressRecipient)) {
                                //Tipe Payment dengan Request dari Orang Lain

                            } else if (TP_TransactionType.equals("RequestPayment") && !RequestFrom.equals(TP_EmailAddressSender) ) {
                                //Tipe Request Payment ke orang lain

                            }
                        }

                    } else {
                        //Toast.makeText(taskpay.this, "Error, No Data in database !", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<transactioninquiry>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            //Jika salah
        }




        //Jika KlikPhoto maka otomatis Photo :
        IVTRPPhoto = (ImageView) findViewById(R.id.IVPhoto);
        IVTRPPhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                if(!TP_StatusPayment.equals("paid")) {

                }
            }
        });

        //klik pay
        btnTPPay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                if (TP_Status.equals("new")){
                    setNewTransactionWithoutRequest();
                } else if (TP_Status.equals("edit")){

                }
            }
        });

    }

    private void TakePicture(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CAMERA_REQUEST);
        }
    }


    String mCurrentPhotoPath;
    Uri photoUri;
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    static final int REQUEST_TAKE_PHOTO = 1;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                //Uri photoURI = FileProvider.getUriForFile(this,
                //        "com.example.android.fileprovider",
                //        photoFile);

                //photoURI = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                //startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }

        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 8;
            //final Bitmap bitmap = BitmapFactory.decodeFile(photoUri.getPath(), options);

            Bitmap mphoto = (Bitmap) data.getExtras().get("data");
            mImageView.setImageBitmap(mphoto);
        }

    }

    private void setNewTransactionWithoutRequest() {
        String T_EmailAddress, EmailAddressSender, EmailAddressTo, TransactionDate, Notes, PaymentPicture = "";
        Double Amount = 0.0;
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        String date = df.format(Calendar.getInstance().getTime());
        final Double xBalanceInfo = 0.0;

        //Define dulu Api Classnya
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        Api api = retrofit.create(Api.class);

        //Cek aapakah ada field yang belum terisi ?

        TP_TransactionType = "payment";
        TP_EmailAddressSender = TP_Username;
        TP_RequestFrom = "";
        TP_TransactionDate = date.toString();
        TP_RequestAmount = 0.0;
        TP_Amount = Double.parseDouble(ETAmount.getText().toString());
        TP_CurrencyID = "IDR";
        TP_Notes = ETNotes.getText().toString();
        TP_CurentBalance = 0.0;
        TP_AccountStatementID = "";
        TP_LastAccountStatementID = "";
        TP_PaymentPicture = "";
        TP_StatusPayment = "paid";
        TP_TransactionID = "";
        TP_IsChannel = "false";

        //Cek dulu ada gak saldonya
        Call<List<Nama>> call = api.getLogin2(TP_Username);

        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                Double Balance;
                Log.e("taskpay",""+CustomerList.size());
                //Jika ada field
                if (CustomerList.size() > 0) {
                    Temp_Amount = CustomerList.get(0).getbalanceinfo();



                    //Jika tidak saldo maka berhenti
                    if (TP_Amount > Temp_Amount) {
                        //Toast.makeText(, "Insufficient Balance. Please Top Up First before do a payment", Toast.LENGTH_LONG).show();
                        Log.e("taskpay","Insufficient Balance. Please Top Up First before do a payment");

                    } else {
                        //Jika ada saldo maka lanjutkan untuk meminta pin lagi

                        SuccessPassword = true;

                        if (SuccessPassword) {

                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(Api.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();
                            Api api = retrofit.create(Api.class);

                            final Double CurrentBalance = Temp_Amount - TP_Amount;
                            Log.e("taskpay","Current Balance "+ CurrentBalance);

                            //Update Balance Info
                            Call<Nama> UBCall = api.setUpdateBalance(TP_Username, CurrentBalance);
                            UBCall.enqueue(new Callback<Nama>() {
                                @Override
                                public void onResponse(Call<Nama> call, Response<Nama> response) {
                                    Log.e("taskpay",""+CurrentBalance);
                                }

                                @Override
                                public void onFailure(Call<Nama> call, Throwable t) {
                                    Log.e("taskpay", t.toString());
                                }
                            });

                            //Create Transaksi
                            Retrofit TIretrofit = new Retrofit.Builder()
                                    .baseUrl(Api.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();
                            Api TIapi = retrofit.create(Api.class);
                            Call<transactioninquiry> TIcall = TIapi.setTransactioninquiry(TP_TransactionType, TP_Username, TP_EmailAddressSender
                                    , TP_EmailAddressRecipient, TP_RequestFrom, TP_TransactionDate, TP_Notes, TP_CurrencyID
                                    , TP_RequestAmount, TP_Amount, TP_StatusPayment, TP_PaymentPicture
                                    , TP_CurentBalance, TP_AccountStatementID, TP_LastAccountStatementID
                                    , TP_TransactionID, TP_IsChannel);
                            TIcall.enqueue(new Callback<transactioninquiry>() {
                                @Override
                                public void onResponse(Call<transactioninquiry> call, Response<transactioninquiry> response) {
                                    //Toast.makeText(taskpay.this, "Transaction has been create successfully", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onFailure(Call<transactioninquiry> call, Throwable t) {

                                }
                            });
                        } else {
                            //Jika Gagal maka infokan password salah

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

}
