package com.example.jeffry.snapaybca;

public class transaction {
	private String amount;
	private String emailaddresssender;
	private String transactiondate;
	private String notes;
	private String accountstatementid;
	private String lastaccountstatementid;
	private String emailaddressrecipient;
	private String emailaddress;
	private Integer id;
	private String currencyid;
	private String transactiontype;
	private String currentbalance;
	private String requestfrom;

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setEmailaddresssender(String emailaddresssender){
		this.emailaddresssender = emailaddresssender;
	}

	public String getEmailaddresssender(){
		return emailaddresssender;
	}

	public void setTransactiondate(String transactiondate){
		this.transactiondate = transactiondate;
	}

	public String getTransactiondate(){
		return transactiondate;
	}

	public void setNotes(String notes){
		this.notes = notes;
	}

	public String getNotes(){
		return notes;
	}

	public void setAccountstatementid(String accountstatementid){
		this.accountstatementid = accountstatementid;
	}

	public String getAccountstatementid(){
		return accountstatementid;
	}

	public void setLastaccountstatementid(String lastaccountstatementid){
		this.lastaccountstatementid = lastaccountstatementid;
	}

	public String getLastaccountstatementid(){
		return lastaccountstatementid;
	}

	public void setEmailaddressrecipient(String emailaddressrecipient){
		this.emailaddressrecipient = emailaddressrecipient;
	}

	public String getEmailaddressrecipient(){
		return emailaddressrecipient;
	}

	public void setEmailaddress(String emailaddress){
		this.emailaddress = emailaddress;
	}

	public String getEmailaddress(){
		return emailaddress;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setCurrencyid(String currencyid){
		this.currencyid = currencyid;
	}

	public String getCurrencyid(){
		return currencyid;
	}

	public void setTransactiontype(String transactiontype){
		this.transactiontype = transactiontype;
	}

	public String getTransactiontype(){
		return transactiontype;
	}

	public void setCurrentbalance(String currentbalance){
		this.currentbalance = currentbalance;
	}

	public String getCurrentbalance(){
		return currentbalance;
	}

	public void setrequestfrom(String requestfrom){
		this.requestfrom = requestfrom;
	}

	public String getrequestfrom(){
		return requestfrom;
	}


	@Override
 	public String toString(){
		return 
			"transaction{" +
			"amount = '" + amount + '\'' + 
			",emailaddresssender = '" + emailaddresssender + '\'' + 
			",transactiondate = '" + transactiondate + '\'' + 
			",notes = '" + notes + '\'' + 
			",accountstatementid = '" + accountstatementid + '\'' + 
			",lastaccountstatementid = '" + lastaccountstatementid + '\'' + 
			",emailaddressrecipient = '" + emailaddressrecipient + '\'' + 
			",emailaddress = '" + emailaddress + '\'' + 
			",id = '" + id + '\'' + 
			",currencyid = '" + currencyid + '\'' + 
			",transactiontype = '" + transactiontype + '\'' + 
			",currentbalance = '" + currentbalance + '\'' + 
			"}";
		}
}
