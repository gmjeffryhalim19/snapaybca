package com.example.jeffry.snapaybca;

public class friend {
	private String emailaddressfriend;
	private String emailaddress;
	private String id;

	public void setEmailaddressfriend(String emailaddressfriend){
		this.emailaddressfriend = emailaddressfriend;
	}

	public String getemailaddressfriend(){
		return emailaddressfriend;
	}

	public void setemailaddress(String emailaddress){
		this.emailaddress = emailaddress;
	}

	public String getemailaddress(){
		return emailaddress;
	}

	public void setid(String id){
		this.id = id;
	}

	public String getid(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"friend{" +
			"emailaddressfriend = '" + emailaddressfriend + '\'' + 
			",emailaddress = '" + emailaddress + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}
