package com.example.jeffry.snapaybca;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.widget.DatePicker;
import android.app.DatePickerDialog;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class createaccount extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);

        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        final EditText xDateOfBirth = (EditText) findViewById(R.id.etDateOfBirth);
        xDateOfBirth.setInputType(0);

        xDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog

                //DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK,this,year,month,day);
                DatePickerDialog datePickerDialog = new DatePickerDialog(createaccount.this,AlertDialog.THEME_HOLO_DARK,
                      new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                xDateOfBirth.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);


                datePickerDialog.show();
            }
        });

        Button CreateAccount = (Button) findViewById(R.id.btnCreate);
        CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAccount();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void setAccount() {
        String FullName, MobileNo, EmailAddress, CompanyCode, CustomerNumber, Password, KTPNo, AccountNo;
        String DateOfBirth;
        Double BalanceInfo=1000000.0;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        Api api = retrofit.create(Api.class);

        EditText ETFullName = (EditText) findViewById(R.id.etFullName);
        EditText ETMobileNo = (EditText) findViewById(R.id.etMobileNo);
        EditText ETDateOfBirth = (EditText) findViewById(R.id.etDateOfBirth);
        EditText ETEmailAddress = (EditText) findViewById(R.id.etusername);
        EditText ETCAPassword = (EditText) findViewById(R.id.ETCAPassword);
        EditText ETKTPNo = (EditText) findViewById(R.id.etktpno);
        EditText ETAccountNo = (EditText) findViewById(R.id.ETAccountNo);

        FullName = ETFullName.getText().toString();
        MobileNo = ETMobileNo.getText().toString();
        EmailAddress = ETEmailAddress.getText().toString();
        Password = ETCAPassword.getText().toString();
        DateOfBirth = ETDateOfBirth.getText().toString();
        CompanyCode = "";
        CustomerNumber = "";
        KTPNo = ETKTPNo.getText().toString();
        AccountNo = ETAccountNo.getText().toString();
        BalanceInfo = 1000000.0;

        //Cek apakah email address atau mobile no sudah pernah terdaftar


        Call <Nama> call = api.setAccount(FullName, MobileNo, DateOfBirth, FullName,EmailAddress,CompanyCode,CustomerNumber,Password, KTPNo
                                        , AccountNo, BalanceInfo );
        call.enqueue(new Callback<Nama>() {
            @Override
            public void onResponse(Call<Nama> call, Response<Nama> response) {
                Toast.makeText(createaccount.this,"User has been created successfully", Toast.LENGTH_LONG).show();
                SuccessCreateAccount();
            }

            @Override
            public void onFailure(Call<Nama> call, Throwable t) {

            }
        });
    }

    private void SuccessCreateAccount() {
        Intent i = null;
        i = new Intent(createaccount.this, MainActivity.class);
        finish();
        startActivity(i);
    }
}
