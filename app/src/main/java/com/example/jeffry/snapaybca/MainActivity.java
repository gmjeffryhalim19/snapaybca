package com.example.jeffry.snapaybca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    static String CustomerName, Username;
    static Double BalanceInfo;
    Boolean SuccessLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Sembunyikan Action Bar
        getSupportActionBar().hide();
        //setTitle("SnaPay BCA - Your reliable E-Wallet");
        //Username = (EditText) findViewById(R.id.etUsername);

        Button Login = (Button) findViewById(R.id.btnLogin);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLogin2();
            }
        });

        TextView LinkCreateAccount = (TextView) findViewById(R.id.tvLinkCreateAccount);
        LinkCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivity.this, createaccount.class);
                startActivity(i);
            }
        });

        TextView ForgotPassword = (TextView) findViewById(R.id.tvLinkForgotPassword);
        ForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(MainActivity.this, forgotpassword.class);
                startActivity(i);
            }
        });

    }

    void ProcessLogin(){
        Intent i = null;
        //i = new Intent(MainActivity.this, contactlist.class);
        i = new Intent(MainActivity.this, mainformactivity.class);
        //Toast.makeText(getApplicationContext(), CustomerName, Toast.LENGTH_LONG).show();
        i.putExtra("customername", CustomerName);
        i.putExtra("username", Username);
        i.putExtra("balanceinfo", BalanceInfo);
        CustomerName ="";
        Username = "";
        BalanceInfo=0.0;
        startActivity(i);
    }


    private void getLogin() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<Nama>> call = api.getLogin();

        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                //Creating an String array for the ListView
                String[] Email = new String[CustomerList.size()];
                String[] Password= new String[CustomerList.size()];
                String[] FullName = new String[CustomerList.size()];
                Double[] xBalanceInfo = new Double[CustomerList.size()];

                EditText ETUsername = (EditText) findViewById(R.id.ETUsername);
                EditText ETPassword = (EditText) findViewById(R.id.ETPassword);
                //looping through all the heroes and inserting the names inside the string array
                for (int i = 0; i < CustomerList.size(); i++) {
                    Email[i] = CustomerList.get(i).getemailaddress();
                    Password[i] = CustomerList.get(i).getpassword();
                    FullName[i] = CustomerList.get(i).getcustomername();
                    xBalanceInfo[i] = CustomerList.get(i).getbalanceinfo();
                    if (Email[i].equals(ETUsername.getText().toString()) && Password[i].equals(ETPassword.getText().toString())) {
                        SuccessLogin = true;
                        Username = Email[i];
                        CustomerName = FullName[i];
                        BalanceInfo = xBalanceInfo[i];
                        //Toast.makeText(getApplicationContext(), CustomerName, Toast.LENGTH_LONG).show();
                        break;
                    }
                }

                if (SuccessLogin) {
                    ETPassword.setText("");
                    SuccessLogin=false;
                    ProcessLogin();
                } else {
                    Toast.makeText(MainActivity.this,"Invalid Username / Password", Toast.LENGTH_LONG).show();
                }
                //displaying the string array into listview
                //listView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, heroes));

            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getLogin2() {

        String criteria = "";
        EditText xETUsername = (EditText) findViewById(R.id.ETUsername);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<Nama>> call = api.getLogin2(xETUsername.getText().toString());

        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();


                EditText ETUsername = (EditText) findViewById(R.id.ETUsername);
                EditText ETPassword = (EditText) findViewById(R.id.ETPassword);
                //Toast.makeText(getApplicationContext(), "ERROR" , Toast.LENGTH_LONG).show();
                //Log.e("MainActivity", "" +CustomerList.size());
                //Creating an String array for the ListView
                String[] Email = new String[CustomerList.size()];
                String[] Password= new String[CustomerList.size()];
                String[] FullName = new String[CustomerList.size()];
                Double[] xBalanceInfo = new Double[CustomerList.size()];


                //looping through all the heroes and inserting the names inside the string array
                for (int i = 0; i < CustomerList.size(); i++) {
                    Email[i] = CustomerList.get(i).getemailaddress();
                    Password[i] = CustomerList.get(i).getpassword();
                    FullName[i] = CustomerList.get(i).getcustomername();
                    xBalanceInfo[i] = CustomerList.get(i).getbalanceinfo();
                    if (Email[i].equals(ETUsername.getText().toString()) && Password[i].equals(ETPassword.getText().toString())) {
                        SuccessLogin = true;
                        Username = Email[i];
                        CustomerName = FullName[i];
                        BalanceInfo = xBalanceInfo[i];
                        //Toast.makeText(getApplicationContext(), i , Toast.LENGTH_LONG).show();
                        break;
                    }
                }

                if (SuccessLogin) {
                    ETPassword.setText("");
                    SuccessLogin=false;
                    ProcessLogin();
                } else {
                    Toast.makeText(MainActivity.this,"Invalid Username / Password", Toast.LENGTH_LONG).show();
                }
                //displaying the string array into listview
                //listView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, heroes));

            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
