package com.example.jeffry.snapaybca;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TextView;
import android.view.Window;
import android.view.WindowManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.view.View;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class taskpay extends AppCompatActivity {

    ImageView IVPhoto;
    static int CAMERA_REQUEST = 1888;
    static int TP_ID;
    static String TP_Username , TP_TransactionType, TP_EmailAddress, TP_EmailAddressTo
                    , TP_RequestFrom, TP_EmailAddressSender, TP_EmailAddressRecipient
                    , TP_TransactionDate, TP_Notes, TP_CurrencyID, TP_PaymentPicture
                    , TP_Status, TP_IsChannel, TP_TypeForm = "";
    static String TP_AccountStatementID, TP_LastAccountStatementID, TP_StatusPayment, TP_TransactionID;
    static Double TP_RequestAmount, TP_Amount, TP_CurentBalance, Temp_Amount, Temp_Amount_Recipient;

    static Boolean SuccessPassword;

    private Double TempAmount;

    EditText ETPayToUsername, ETAmount, ETNotes;
    Button btnTPPay, btnTPReject;

    private Button takePictureButton;
    private ImageView imageView;
    private Uri file;

    public static String ImageFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taskpay);

        TP_StatusPayment="";
        Temp_Amount = 0.0;

        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        Bundle bundle = getIntent().getExtras();
        TP_ID = Integer.parseInt(bundle.getString("id"));
        TP_Username = bundle.getString("username");
        TP_TransactionType  = bundle.getString ("description");
        TP_EmailAddressRecipient = bundle.getString("recipient");
        TP_RequestFrom = bundle.getString("requestfrom");
        TP_Status = bundle.getString("status");
        TP_TypeForm = bundle.getString("typeform");

        TextView tvPaymentTo = (TextView) findViewById(R.id.tvPaymentTo);
        EditText ETPayToUsername = (EditText) findViewById(R.id.etTo);
        EditText ETAmount = (EditText) findViewById(R.id.etAmount);
        EditText ETNotes = (EditText) findViewById(R.id.etNotes);
        Button btnTPReject = (Button) findViewById(R.id.btnTPReject);
        Button btnTPPay = (Button) findViewById(R.id.btnTPPay);


        ETPayToUsername.setInputType(InputType.TYPE_NULL);
        ETPayToUsername.setKeyListener(null);

        if (TP_Status.equals("new")){
            //Jika Baru, maka set saja payto username
            //Cek lagi apakah ini requestpayment atau payment
            btnTPReject.setVisibility(View.GONE);
            //Jika Paymentonly
            if (TP_TypeForm.equals("paymentonly")) {
                ETPayToUsername.setText(TP_EmailAddressRecipient);

            } else if (TP_TypeForm.equals("requestpaymentonly")) {
                //Jika RequestPaymentOnly
                btnTPPay.setText("Request Payment");
                tvPaymentTo.setText("Request To Username");
                ETPayToUsername.setText(TP_EmailAddressRecipient);
            }

        } else if (TP_Status.equals("edit")) {

            btnTPReject.setVisibility(View.GONE);
            // Jika edit, maka load semua data
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                    .build();

            Api api = retrofit.create(Api.class);

            Call<List<transactioninquiry>> call = api.getTransactioninquiry2(TP_ID);

            call.enqueue(new Callback<List<transactioninquiry>>()  {
                @Override
                public void onResponse(Call<List<transactioninquiry>> call, Response<List<transactioninquiry>> response) {
                    List<transactioninquiry> TransactionList = response.body();


                    // Jika memang ada hasil inquiry maka load baris pertama saja
                    if (TransactionList.size() > 0 ) {
                        String TransactionType,EmailAddress,EmailAddressSender, EmailAddressRecipient,RequestFrom
                                ,TransactionDate,Notes,CurrencyID, PaymentPicture, StatusPayment = "";
                        Double RequestAmount,Amount = 0.0;

                        int i = 0;
                        TransactionType = TransactionList.get(i).getTransactiontype();
                        EmailAddress = TransactionList.get(i).getEmailaddress();
                        EmailAddressSender = TransactionList.get(i).getEmailaddresssender();
                        EmailAddressRecipient = TransactionList.get(i).getEmailaddressrecipient();
                        RequestFrom = TransactionList.get(i).getRequestfrom();
                        TransactionDate = TransactionList.get(i).getTransactiondate();
                        Notes = TransactionList.get(i).getNotes();
                        CurrencyID = TransactionList.get(i).getCurrencyid();
                        RequestAmount = TransactionList.get(i).getRequestamount();
                        Amount = TransactionList.get(i).getAmount();
                        PaymentPicture = TransactionList.get(i).getPaymentpicture();
                        StatusPayment = TransactionList.get(i).getStatuspayment();
                        ImageFilePath = TransactionList.get(i).getPaymentpicture();

                        //Bitmap bmImg = BitmapFactory.decodeFile(ImageFilePath);
                        //imageView = (ImageView) findViewById(R.id.IVPhoto);
                        //imageView.setImageBitmap(bmImg);

                        EditText ETPayToUsername = (EditText) findViewById(R.id.etTo);
                        EditText ETAmount = (EditText) findViewById(R.id.etAmount);
                        EditText ETNotes = (EditText) findViewById(R.id.etNotes);
                        TextView tvPaymentTo = (TextView) findViewById(R.id.tvPaymentTo);
                        Button btnTPReject = (Button) findViewById(R.id.btnTPReject);
                        Button btnTPPay = (Button) findViewById(R.id.btnTPPay);

                        //Jika Tipe Paymentwithrequestpayment
                        if (TP_TypeForm.equals("paymentwithrequest")) {
                            if (TP_Username.equals(EmailAddressSender)) {
                                tvPaymentTo.setText("Request To Username");
                                ETPayToUsername.setText(EmailAddressRecipient);
                            } else {
                                ETPayToUsername.setText(EmailAddressSender);
                            }
                        } else if(TP_TypeForm.equals("paymentonly")) {
                            ETPayToUsername.setText(EmailAddressRecipient);
                        } else if (TP_TypeForm.equals("requestpaymentonly")) {
                            tvPaymentTo.setText("Request To Username");
                            ETPayToUsername.setText(EmailAddressRecipient);
                        }

                        ETAmount.setText(Amount.toString());
                        ETNotes.setText(Notes);

                        //Load Gambar

                        //Jika status sudah complete maka ga bisa edit lagi
                        if(StatusPayment.equals("paid") || StatusPayment.equals("rejected")){

                            ETAmount.setInputType(InputType.TYPE_NULL);
                            ETAmount.setKeyListener(null);

                            ETNotes.setInputType(InputType.TYPE_NULL);
                            ETNotes.setKeyListener(null);

                            btnTPPay.setVisibility(View.GONE);
                            btnTPReject.setVisibility(View.GONE);

                        } else {
                            Log.e("taskpay", "1" + StatusPayment + " VS " + TP_Username + " VS " + EmailAddressSender);
                            if (StatusPayment.equals("requested") && TP_Username.equals(EmailAddressSender)) {

                                //Cek mau load form apa (Payment atau Request Payment)
                                if (TP_TypeForm.equals("paymentwithrequest")) {
                                    //Jika dia adalah payment dengan request payment, maka munculkanlah Pay atau Reject
                                    btnTPPay.setVisibility(View.VISIBLE);
                                    btnTPReject.setVisibility(View.VISIBLE);
                                    ETAmount.setInputType(InputType.TYPE_NULL);
                                    ETAmount.setKeyListener(null);

                                } else {
                                    btnTPPay.setVisibility(View.GONE);
                                }
                            } else {
                                btnTPReject.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {
                        Toast.makeText(taskpay.this, "Error, No Data in database !", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<transactioninquiry>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            //Jika salah
        }

        //takePictureButton = (Button) findViewById(R.id.button_image);
        //imageView = (ImageView) findViewById(R.id.IVPhoto);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            takePictureButton.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }


        //Jika KlikPhoto maka otomatis Photo :
        IVPhoto = (ImageView) findViewById(R.id.IVPhoto);
        IVPhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                if(!TP_StatusPayment.equals("paid") ||!TP_StatusPayment.equals("rejected")||!TP_StatusPayment.equals("requested")) {

                }
            }
        });

        //klik pay
        btnTPPay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                if (TP_Status.equals("new")){
                    if(TP_TypeForm.equals("paymentonly")){
                        Log.e("taskpay", TP_TypeForm);
                        setNewTransactionWithoutRequest();
                    } else if (TP_TypeForm.equals("requestpaymentonly")){
                        Log.e("taskpay", TP_TypeForm);
                        setNewTransactionWithRequest();
                    }
                } else if (TP_Status.equals("edit")){

                }
            }
        });

        //klik pay
        btnTPReject.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                RejectTransaction();
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void RejectTransaction() {

        //Define dulu Api Classnya
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        Api api = retrofit.create(Api.class);

        Call<transactioninquiry> Call = api.setReject(TP_ID, "rejected");
        Call.enqueue(new Callback<transactioninquiry>() {
            @Override
            public void onResponse(Call<transactioninquiry> call, Response<transactioninquiry> response) {
                Log.e("taskpay", "Rejected");
            }

            @Override
            public void onFailure(Call<transactioninquiry> call, Throwable t) {
                Log.e("taskpay", t.toString());
            }
        });

    }


    private void setNewTransactionWithoutRequest() {
        String T_EmailAddress, EmailAddressSender, EmailAddressTo, TransactionDate, Notes, PaymentPicture = "";
        Double Amount = 0.0;
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        String date = df.format(Calendar.getInstance().getTime());
        final Double xBalanceInfo = 0.0;

        EditText ETAmount = (EditText) findViewById(R.id.etAmount);
        EditText ETNotes = (EditText) findViewById(R.id.etNotes);



        //Cek aapakah ada field yang belum terisi ?

        TP_TransactionType = "payment";
        TP_EmailAddressSender = TP_Username;
        TP_RequestFrom = TP_Username;
        TP_TransactionDate = date.toString();
        TP_RequestAmount = 0.0;
        TP_Amount = Double.parseDouble(ETAmount.getText().toString());
        TP_CurrencyID = "IDR";
        TP_Notes = ETNotes.getText().toString();
        TP_CurentBalance = 0.0;
        TP_AccountStatementID = "";
        TP_LastAccountStatementID = "";
        TP_PaymentPicture = "";
        TP_StatusPayment = "paid";
        TP_TransactionID = "";
        TP_IsChannel = "false";

        //Ambil dulu data si Recipient
        //Lanjutkan ambil data dari EmailAddressRecipient
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        //Cek dulu ada gak saldonya
        Call<List<Nama>> call = api.getLogin2(TP_EmailAddressRecipient);
        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();
                //Log.e("taskpay","RCP 2a" + TP_EmailAddressRecipient + "  " + Temp_Amount_Recipient);

                if(CustomerList.size() > 0) {
                    Temp_Amount_Recipient = CustomerList.get(0).getbalanceinfo();
                    //Log.e("taskpay","RCP 3" + Temp_Amount_Recipient);
                }
                //Jika berhasil maka update data ke recipient
            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Log.e("taskpay", t.toString());
            }
        });

        //Define dulu Api Classnya
        /**
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
         **/
        Api xapi = retrofit.create(Api.class);
        //Cek dulu ada gak saldonya
        Call<List<Nama>> xcall = xapi.getLogin2(TP_Username);
        xcall.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                Double Balance;
                Log.e("taskpay",""+CustomerList.size());
                //Jika ada field
                if (CustomerList.size() > 0) {
                    taskpay.Temp_Amount = CustomerList.get(0).getbalanceinfo();

                    //Jika tidak saldo maka berhenti
                    if (TP_Amount > taskpay.Temp_Amount) {
                        Toast.makeText( taskpay.this, "Insufficient Balance. Please Top Up First before do a payment", Toast.LENGTH_LONG).show();
                        //Log.e("taskpay","Insufficient Balance. Please Top Up First before do a payment");

                    } else {
                        /**
                        //Lanjutkan ambil data dari EmailAddressRecipient
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(Api.BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                .build();
                        Api api = retrofit.create(Api.class);
                        Api RCPapi = retrofit.create(Api.class);
                        Call<List<Nama>> RCPcall = RCPapi.getLogin2cadangan(TP_EmailAddressRecipient);
                        Log.e("taskpay","RCP 1" + TP_EmailAddressRecipient);
                        Log.e("taskpay","RCP 1" + Temp_Amount_Recipient);
                        RCPcall.enqueue(new Callback<List<Nama>>() {

                            @Override
                            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response){
                                Log.e("taskpay","RCP 2a" + Temp_Amount_Recipient);
                                List<Nama> CustomerList = response.body();
                                Log.e("taskpay","RCP 2" + Temp_Amount_Recipient);

                                if(CustomerList.size() > 0) {
                                    Temp_Amount_Recipient = CustomerList.get(0).getbalanceinfo();
                                    Log.e("taskpay","RCP 3" + Temp_Amount_Recipient);
                                }
                                //Jika berhasil maka update data ke recipient
                            }

                            @Override
                            public void onFailure(Call<List<Nama>> call, Throwable t) {
                                Log.e("taskpay", t.toString());
                            }
                        });
                        **/

                        //Update Balance Info
                        //Jika ada saldo maka lanjutkan untuk meminta pin lagi

                        SuccessPassword = true;
                        if (SuccessPassword) {

                            final Double CurrentBalance = Temp_Amount - TP_Amount;
                            Log.e("taskpay","Current Balance "+ CurrentBalance);
                            Log.e("taskpay", "Update Recipient " + (Temp_Amount_Recipient) );
                            Log.e("taskpay", "TP Amount " + (TP_Amount));

                            //Update Balance Info
                            Retrofit UBretrofit = new Retrofit.Builder()
                                    .baseUrl(Api.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();
                            Api UBapi = UBretrofit.create(Api.class);
                            Call<Nama> UBcall = UBapi.setUpdateBalance(TP_Username, Temp_Amount - TP_Amount);
                            UBcall.enqueue(new Callback<Nama>() {
                                @Override
                                public void onResponse(Call<Nama> call, Response<Nama> response) {
                                    Log.e("taskpay",""+CurrentBalance);

                                    /**
                                    //Update lagi emailaddressrecipient
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl(Api.BASE_URL)
                                            .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                            .build();
                                    Api api = retrofit.create(Api.class);
                                    Call<Nama> UBCall = api.setUpdateBalance(TP_Username, Temp_Amount_Recipient + TP_Amount);
                                    Log.e("taskpay", "Update Recipient " + (Temp_Amount_Recipient + TP_Amount) );**/

                                }

                                @Override
                                public void onFailure(Call<Nama> call, Throwable t) {
                                    Log.e("taskpay", t.toString());
                                }
                            });

                            //Temp_Amount_Recipient = 0.0;

                            //Update Balance Info Recipient
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(Api.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();
                            Api xapi = retrofit.create(Api.class);
                            Call<Nama> xcall = xapi.setUpdateBalance(TP_EmailAddressRecipient, Temp_Amount_Recipient + TP_Amount );
                            xcall.enqueue(new Callback<Nama>() {
                                @Override
                                public void onResponse(Call<Nama> call, Response<Nama> response) {
                                    Log.e("taskpay", "dalam " + taskpay.Temp_Amount);
                                }

                                @Override
                                public void onFailure(Call<Nama> call, Throwable t) {

                                }
                            });

                            //Create Transaksi
                            Retrofit TIretrofit = new Retrofit.Builder()
                                    .baseUrl(Api.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();
                            Api TIapi = TIretrofit.create(Api.class);
                            Call<transactioninquiry> TIcall = TIapi.setTransactioninquiry(TP_TransactionType, TP_Username, TP_EmailAddressSender
                                    , TP_EmailAddressRecipient, TP_RequestFrom, TP_TransactionDate, TP_Notes, TP_CurrencyID
                                    , TP_RequestAmount, TP_Amount, TP_StatusPayment, taskpay.TP_PaymentPicture
                                    , TP_CurentBalance, TP_AccountStatementID, TP_LastAccountStatementID
                                    , TP_TransactionID, TP_IsChannel);
                            TIcall.enqueue(new Callback<transactioninquiry>() {
                                @Override
                                public void onResponse(Call<transactioninquiry> call, Response<transactioninquiry> response) {
                                    Toast.makeText(taskpay.this, "Transaction has been create successfully", Toast.LENGTH_LONG).show();
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<transactioninquiry> call, Throwable t) {

                                }
                            });
                        } else {
                            //Jika Gagal maka infokan password salah

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void LoadImage() {

    }


    private void setNewTransactionWithRequest() {
        String T_EmailAddress, EmailAddressSender, EmailAddressTo, TransactionDate, Notes, PaymentPicture = "";
        Double Amount = 0.0;
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
        String date = df.format(Calendar.getInstance().getTime());
        final Double xBalanceInfo = 0.0;

        EditText ETAmount = (EditText) findViewById(R.id.etAmount);
        EditText ETNotes = (EditText) findViewById(R.id.etNotes);
        //Cek aapakah ada field yang belum terisi ?

        TP_TransactionType = "requestpayment";
        TP_EmailAddressSender = TP_Username;
        TP_RequestFrom = TP_Username;
        TP_TransactionDate = date.toString();
        TP_RequestAmount = Double.parseDouble(ETAmount.getText().toString());
        TP_Amount = Double.parseDouble(ETAmount.getText().toString());
        TP_CurrencyID = "IDR";
        TP_Notes = ETNotes.getText().toString();
        TP_CurentBalance = 0.0;
        TP_AccountStatementID = "";
        TP_LastAccountStatementID = "";
        TP_PaymentPicture = "";
        TP_StatusPayment = "requested";
        TP_TransactionID = "";
        TP_IsChannel = "false";


                        //Minta Password ulang
                        SuccessPassword = true;

                        if (SuccessPassword) {

                            //Create Transaksi
                            Retrofit TIretrofit = new Retrofit.Builder()
                                    .baseUrl(Api.BASE_URL)
                                    .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                                    .build();
                            Api TIapi = TIretrofit.create(Api.class);
                            Call<transactioninquiry> TIcall = TIapi.setTransactioninquiry(TP_TransactionType, TP_Username, TP_EmailAddressSender
                                    , TP_EmailAddressRecipient, TP_RequestFrom, TP_TransactionDate, TP_Notes, TP_CurrencyID
                                    , TP_RequestAmount, TP_Amount, TP_StatusPayment, TP_PaymentPicture
                                    , TP_CurentBalance, TP_AccountStatementID, TP_LastAccountStatementID
                                    , TP_TransactionID, TP_IsChannel);
                            TIcall.enqueue(new Callback<transactioninquiry>() {
                                @Override
                                public void onResponse(Call<transactioninquiry> call, Response<transactioninquiry> response) {
                                    Toast.makeText(taskpay.this, "Transaction has been create successfully", Toast.LENGTH_LONG).show();
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<transactioninquiry> call, Throwable t) {

                                }
                            });
                        } else {
                            //Jika Gagal maka infokan password salah

                        }
                    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //takePictureButton.setEnabled(true);
            }
        }
    }

    public void takePicture(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

        startActivityForResult(intent, 100);
    }

    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        TP_PaymentPicture= mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg";
        Log.e("taskpay", TP_PaymentPicture);
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                IVPhoto.setImageURI(file);
            }
        }
    }

    public void takePictures(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

        startActivityForResult(intent, 100);
    }
}
