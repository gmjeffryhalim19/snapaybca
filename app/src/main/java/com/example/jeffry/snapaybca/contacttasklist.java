package com.example.jeffry.snapaybca;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ListView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.util.Log;

import android.widget.Button;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class contacttasklist extends AppCompatActivity {
    static String xTransactionEmailAddress, xTransactionEmailAddressFriend;
    String [] taskid, taskdescription;
    //private ListView mainListView ;
    //private ArrayAdapter<String> listAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacttasklist);

        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        Bundle bundle = getIntent().getExtras();
        xTransactionEmailAddress = bundle.getString("friendemailaddress");
        xTransactionEmailAddressFriend = bundle.getString("friendemailaddressfriend");

        //final String Username = text;
        //Toast.makeText(contacttasklist.this, text, Toast.LENGTH_LONG).show();
        Log.e("contacttasklist", "Username"+xTransactionEmailAddress);
        Log.e("contacttasklist", "UsernameFriend"+xTransactionEmailAddressFriend);
        getTaskList2();


        //Process Payment
        Button btnPayment = (Button) findViewById(R.id.btnTLPay);
        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(contacttasklist.this, taskpay.class);
                i.putExtra("id","0");
                i.putExtra("username", xTransactionEmailAddress);
                i.putExtra("description", "payment");
                i.putExtra("recipient", xTransactionEmailAddressFriend);
                i.putExtra("requestfrom", "");
                i.putExtra("status", "new");
                i.putExtra("typeform", "paymentonly");
                startActivity(i);
                getTaskList2();
            }
        });

        //Process Request Payment
        Button btnRequestPayment = (Button) findViewById(R.id.btnTLRequestPayment);
        btnRequestPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(contacttasklist.this, taskpay.class);
                i.putExtra("id","0");
                i.putExtra("username", xTransactionEmailAddress);
                i.putExtra("description", "requestpayment");
                i.putExtra("recipient", xTransactionEmailAddressFriend);
                i.putExtra("requestfrom", xTransactionEmailAddressFriend);
                i.putExtra("status", "new");
                i.putExtra("typeform", "requestpaymentonly");
                startActivity(i);
                getTaskList2();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void getTaskList2() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        Log.e("contacttasklist", xTransactionEmailAddress);

        Call<List<transaction>> call = api.getTransaction2(xTransactionEmailAddress, xTransactionEmailAddressFriend);
        call.enqueue(new Callback<List<transaction>>() {
            @Override
            public void onResponse(Call<List<transaction>> call, retrofit2.Response<List<transaction>> response) {
                List<transaction> TransactionList = response.body();

                //Toast.makeText(getApplicationContext(), "ERROR" , Toast.LENGTH_LONG).show();
                Log.e("MainActivity", "" +TransactionList.size());
                //Creating an String array for the ListView
                int[] Transaction_ID = new int[TransactionList.size()];
                String[] Transaction_TransactionType = new String[TransactionList.size()];
                String[] Transaction_EmailAddress = new String[TransactionList.size()];
                String[] Transaction_EmailAddressSender = new String[TransactionList.size()];
                String[] Transaction_EmailAddressRecipient = new String[TransactionList.size()];
                String[] Transaction_RequestFrom = new String[TransactionList.size()];
                //String[] Transaction_TransactionDate = new String[TransactionList.size()];
                //String[] Transaction_CurrencyID = new String[TransactionList.size()];
                //Double[] Transaction_Amount = new Double[TransactionList.size()];

                //Log.e("contactlist", ""+FriendList.size());
                //Toast.makeText(getApplicationContext(), "" + FriendList.size() , Toast.LENGTH_LONG).show();
                //looping untuk mencari
                //for (int i = 0; i < FriendList.size(); i++) {
                if (TransactionList.size() > 0) {

                    //Lakukan Refresh Grid ya
                    // Array of strings for ListView Title
                    List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

                    //Lakukan load data hasil database ke dalam Array
                    for (int i=0; i < TransactionList.size() ; i++) {
                        Transaction_ID[i] = TransactionList.get(i).getId();
                        Transaction_TransactionType[i] = TransactionList.get(i).getTransactiontype();
                        Transaction_RequestFrom[i] = TransactionList.get(i).getrequestfrom();

                        //Add Array
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("listview_taskid", ""+Transaction_ID[i]);
                        hm.put("listview_taskdescription", Transaction_TransactionType[i]);
                        hm.put("listview_requestfrom", Transaction_RequestFrom[i]);
                        aList.add(hm);
                    }

                    String[] from = {"listview_taskid", "listview_taskdescription", "listview_requestfrom"};
                    int[] to = {R.id.listview_item_taskid, R.id.listview_item_taskdescription, R.id.listview_item_emailaddress};

                    SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.simplerow, from, to);
                    ListView androidListView = (ListView) findViewById(R.id.lvTaskList);
                    androidListView.setAdapter(simpleAdapter);

                    //Jika diklik, maka tampilkan sesuai dengan Pay atau Request Pay
                    androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String clickedTaskID = ((TextView) view.findViewById(R.id.listview_item_taskid)).getText().toString();
                            String clickedTaskDescription = ((TextView) view.findViewById(R.id.listview_item_taskdescription)).getText().toString();
                            String clickedTaskEmailAddress = ((TextView) view.findViewById(R.id.listview_item_emailaddress)).getText().toString();
                            Intent i = null;
                            i = new Intent(contacttasklist.this, taskpay.class);
                            if (clickedTaskDescription.equals("payment")) {
                                if (clickedTaskEmailAddress.equals(xTransactionEmailAddress)){
                                    //Jika seperti ini maka artinya payment tanpa request
                                    i.putExtra("typeform", "paymentonly");
                                    Log.e("contacttasklist","paymentonly");
                                } else {
                                    //Jika seperti ini maka artinya payment dengan request
                                    i.putExtra("typeform", "paymentwithrequest");
                                    Log.e("contacttasklist","paymentwithrequest");
                                }

                            } else if (clickedTaskDescription.equals("requestpayment")){
                                //Jika RequestPayment maka cek lagi apakah request from sama dengan orang yang buka
                                //Jika sama maka dianggap sudah complete
                                if (clickedTaskEmailAddress.equals(xTransactionEmailAddress)){
                                    i.putExtra("typeform", "requestpaymentonly");
                                    Log.e("contacttasklist","requestpaymentonly");
                                } else  {
                                    i.putExtra("typeform", "paymentwithrequest");
                                    Log.e("contacttasklist","paymentwithrequest");
                                }
                            } else {
                                //Error
                            }

                            i.putExtra("id", clickedTaskID);
                            i.putExtra("description", clickedTaskDescription);
                            i.putExtra("username", xTransactionEmailAddress);
                            i.putExtra("recipient", xTransactionEmailAddressFriend);
                            i.putExtra("requestfrom", clickedTaskEmailAddress);
                            i.putExtra("status", "edit");
                            startActivity(i);
                            getTaskList2();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<transaction>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void ProcessPayment(){

    }


}
