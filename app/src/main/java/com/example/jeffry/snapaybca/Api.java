package com.example.jeffry.snapaybca;

/**
 * Created by Jeffry on 13/11/2017.
 */

import android.support.annotation.IntegerRes;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Api {
    String BASE_URL_WEBHOST = "https://demosnapay.000webhostapp.com/apisnapay/index.php/";
    String BASE_URL = "http://182.16.165.82/apisnapay/index.php/";


    @GET("login")
    Call<List<Nama>> getLogin();

    @GET("login")
    Call<List<Nama>> getLogin2(@Query("emailaddress") String phpemailaddress);

    @GET("logincadangan")
    Call<List<Nama>> getLogin2cadangan(@Query("emailaddress") String phpemailaddress);

    @GET("friend")
    Call<List<friend>> getFriendList();
    @GET("friend")
    Call<List<friend>> getFriendList2(@Query("emailaddress") String phpemailaddress);
    @GET("friend2")
    Call<List<friend>> getFriendList3(@Query("emailaddress") String phpemailaddress
                                        ,@Query("emailaddressfriend") String phpemailaddressfriend
                                        );

    @GET("transaction")
    Call<List<transaction>> getTransaction2(@Query("emailaddress") String emailaddress
                                            ,@Query("emailaddressfriend") String emailaddressfriend
                                            );

    @GET("transactioninquiry")
    Call<List<transactioninquiry>> getTransactioninquiry2(@Query("id") int id);


    //=================================POST=====================================================
    //Untuk php Login
    @FormUrlEncoded
    @POST("login")
    Call<Nama> setAccount(
            @Field("nama") String nama,
            @Field("nomor") String nomor,
            @Field("dateofbirth") String dateofbirth,
            @Field("customername") String customername,
            @Field("emailaddress") String emailaddress,
            @Field("companycode") String companycode,
            @Field("customernumber") String customernumber,
            @Field("password") String password,
            @Field("ktpno") String ktpno,
            @Field("accountno") String accountno,
            @Field("balanceinfo") Double balanceinfo
            );

    @FormUrlEncoded
    @POST("friend")
    Call<friend> setFriend(
            @Field("emailaddress") String friend_emailaddress
            ,@Field("emailaddressfriend") String friend_emailaddressfriend
            );

    @FormUrlEncoded
    @POST("transactioninquiry")
    Call<transactioninquiry> setTransactioninquiry(
            @Field("transactiontype") String transactiontype,
            @Field("emailaddress") String emailaddress,
            @Field("emailaddresssender") String emailaddresssender,
            @Field("emailaddressrecipient") String emailaddressrecipient,
            @Field("requestfrom") String requestfrom,
            @Field("transactiondate") String transactiondate,
            @Field("notes") String notes,
            @Field("currencyid") String currencyid,
            @Field("requestamount") Double requestamount,
            @Field("amount") Double amount,
            @Field("statuspayment") String statuspayment,
            @Field("paymentpicture") String paymentpicture,
            @Field("currentbalance") Double currentbalance,
            @Field("accountstatementid") String accountstatementid,
            @Field("lastaccountstatementid") String lastaccountstatementid,
            @Field("transactionid") String transactionid,
            @Field("ischannel") String ischannel
    );
    //=======================================================================================================

    //=================================================PUT=======================================================
    //Untuk php Updateprofile


    //Untuk php Updatebalance
    @FormUrlEncoded
    @PUT("updatebalance")
    Call<Nama> setUpdateBalance(@Field("emailaddress") String emailaddress,
                                @Field("balanceinfo") Double balanceinfo
                                );

    @FormUrlEncoded
    @PUT("transactionreject")
    Call<transactioninquiry> setReject(@Field("id") int id,
                                @Field("statuspayment") String statuspayment
    );
    //============================================================================================================
}
