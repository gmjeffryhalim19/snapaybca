package com.example.jeffry.snapaybca;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.content.Context;
import android.widget.ImageView;
import android.app.Dialog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.util.Log;


public class contactlist extends AppCompatActivity {

    final Context context = this;
    private Button button;
    static String xUsername, xCustomerName;
    static String arrusername[];
    public static Boolean Exist;
    EditText etfriendemailaddressfriend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactlist);

        etfriendemailaddressfriend = (EditText) findViewById(R.id.etfriendemailaddressfriend);
        etfriendemailaddressfriend.setText("");

        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");

        //Ambil data dulu bos
        Intent intent = getIntent();

        xCustomerName = intent.getStringExtra("customername");
        xUsername = intent.getStringExtra("username");

        getFriendList3();

        //Set Click ibaddaccount muncul layar add
        ImageButton ibaddaccount = (ImageButton) findViewById(R.id.ibaddaccount);
        ibaddaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etfriendemailaddressfriend.getText().toString().trim().length() == 0) {
                    Log.e("contactlist", "aaa"+etfriendemailaddressfriend.getText().toString());
                    Toast.makeText(getApplicationContext(), "Please fill the username", Toast.LENGTH_SHORT).show();
                } else {
                    getLogin2();
                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void getLogin2(){
        String FriendPopup_EmailAddress, FriendPopup_EmailAddressFriend;
        Boolean duplicate = false;
        Exist = true;

        EditText etfriendemailaddressfriend = (EditText) findViewById(R.id.etfriendemailaddressfriend);
        FriendPopup_EmailAddressFriend = etfriendemailaddressfriend.getText().toString();
        FriendPopup_EmailAddress = xUsername;

        //Cek apakah exisst ga usernamenya di Database
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);


        /**
 *  Call<List<Nama>> call = api.getLogin2(etfriendemailaddressfriend.getText().toString());
        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, transaction<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                Log.e("contactlist","0"+Exist);
                //Jika CustomerList ada, maka artinya Exist
                if (CustomerList.size() > 0) {
                    Exist = true;
                } else {
                    Exist = false;
                }

                Log.e("contactlist","1"+Exist);
            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Exist = false;
            }
        });

        Log.e("contactlist","2"+Exist);
        //Toast.makeText(contactlist.this, ""+Exist, Toast.LENGTH_LONG).show();
        **/

        //Cek Boolaen Existnya apakaha da atau tidak
        if (Exist) {
            //Cek apakah email address atau mobile no sudah pernah terdaftar
            for (int i = 0; i < contactlist.arrusername.length; i++) {
                if (FriendPopup_EmailAddressFriend.equals(contactlist.arrusername[i])) {
                    duplicate = true;
                    break;
                }
            }

        //Jika sudah duplikat maka tidak jalankan dibawah ini
            if (duplicate) {
                Toast.makeText(contactlist.this, "User Duplicate / Has been Added before", Toast.LENGTH_LONG).show();
            } else {
                Call<friend> xcall = api.setFriend(contactlist.xUsername, FriendPopup_EmailAddressFriend);
                xcall.enqueue(new Callback<friend>() {
                    @Override
                    public void onResponse(Call<friend> call, Response<friend> response) {
                            Toast.makeText(contactlist.this, "User has been added to friend list", Toast.LENGTH_LONG).show();
                            getFriendList3();
                            }

                    @Override
                    public void onFailure(Call<friend> call, Throwable t) {
                            Toast.makeText(contactlist.this, t.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }

        } else {
            Toast.makeText(contactlist.this, "Username not exist !", Toast.LENGTH_LONG).show();
        }
    }

    private void ProcessAdd2(){
        /**
         // custom dialog

         dialog.setContentView(R.layout.activity_addaccountpopup);
         dialog.setTitle("Add Account");

         Button confirm = (Button) dialog.findViewById(R.id.btnconfirm);
         confirm.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        String FriendPopup_EmailAddress, FriendPopup_EmailAddressFriend;
        EditText etfriendpopupeusername = (EditText) dialog.findViewById(R.id.etfriendpopupusername);
        FriendPopup_EmailAddressFriend = etfriendpopupeusername.getText().toString();

        Boolean duplicate = false;
        //Cek apakah exisst ga ussernamenya di Database
        Log.e("contactlist","-2"+Exist);
        Log.e("contactlist","-1"+FriendPopup_EmailAddressFriend);

        Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(Api.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
        .build();
        Api api = retrofit.create(Api.class);
        Call<List<Nama>> call = api.getLogin2(FriendPopup_EmailAddressFriend);
        call.enqueue(new Callback<List<Nama>>() {
        @Override
        public void onResponse(Call<List<Nama>> call, transaction<List<Nama>> response) {
        List<Nama> CustomerList = response.body();

        Log.e("contactlist","0"+Exist);
        //Jika CustomerList ada, maka artinya Exist
        if (CustomerList.size() > 0) {
        Exist = true;
        } else {
        Exist = false;
        }

        Log.e("contactlist","1"+Exist);
        }

        @Override
        public void onFailure(Call<List<Nama>> call, Throwable t) {
        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
        Exist = false;
        }
        });

        Log.e("contactlist","2"+Exist);
        Toast.makeText(contactlist.this, ""+Exist, Toast.LENGTH_LONG).show();

        //Cek Boolaen Existnya apakaha da atau tidak
        if (Exist) {
        //Cek apakah email address atau mobile no sudah pernah terdaftar
        for (int i = 0; i < contactlist.arrusername.length; i++) {
        if (FriendPopup_EmailAddressFriend.equals(contactlist.arrusername[i])) {
        duplicate = true;
        break;
        }
        }

        //Jika sudah duplikat maka tidak jalankan dibawah ini
        if (duplicate) {
        Toast.makeText(contactlist.this, "User Duplicate / Has been Added before", Toast.LENGTH_LONG).show();
        dialog.dismiss();
        } else {
        Call<friend> xcall = api.setFriend(contactlist.xUsername, FriendPopup_EmailAddressFriend);
        xcall.enqueue(new Callback<friend>() {
        @Override
        public void onResponse(Call<friend> call, transaction<friend> response) {
        Toast.makeText(contactlist.this, "User has been added to friend list", Toast.LENGTH_LONG).show();
        dialog.dismiss();
        getFriendList2();
        }

        @Override
        public void onFailure(Call<friend> call, Throwable t) {
        Toast.makeText(contactlist.this, t.toString(), Toast.LENGTH_LONG).show();
        dialog.dismiss();
        }
        });
        }
        } else {
        Toast.makeText(contactlist.this, "Username not exist !", Toast.LENGTH_LONG).show();
        dialog.dismiss();
        }
        }
        });

         dialog.show();
         **/
    }


    private void getFriendList2() {

        String criteria = "";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<friend>> call = api.getFriendList3(xUsername, xUsername);

        call.enqueue(new Callback<List<friend>>() {
            @Override
            public void onResponse(Call<List<friend>> call, retrofit2.Response<List<friend>> response) {
                List<friend> FriendList = response.body();

                String[] Friend_EmailAddress = new String[FriendList.size()];
                String[] Friend_EmailAddressFriend = new String[FriendList.size()];
                arrusername = new String[FriendList.size()];

                if (FriendList.size() > 0) {

                    //Lakukan Refresh Grid ya
                    // Array of strings for ListView Title
                    String[] listviewTitle = new String[FriendList.size()];
                    int[] listviewImage = new int[FriendList.size()];
                    String[] listviewShortDescription = new String[FriendList.size()];

                    for(int i = 0; i < FriendList.size(); i++) {
                        Log.e("contactlist", ""+xUsername+" VS " + FriendList.get(i).getemailaddress());
                        if (FriendList.get(i).getemailaddress().equals(xUsername)) {
                            listviewTitle[i] = FriendList.get(i).getemailaddressfriend();
                        }else {
                            listviewTitle[i] = FriendList.get(i).getemailaddress();
                        }

                        //sementara tetap tambah dengan contact photo dulu
                        listviewImage[i] = R.drawable.contacts;
                        //listviewShortDescription[i] = "";
                    }

                    arrusername = listviewTitle;

                    List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

                    for (int i = 0; i < FriendList.size() ; i++) {
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("listview_title", listviewTitle[i]);
                        //hm.put("listview_discription", listviewShortDescription[i]);
                        hm.put("listview_image", Integer.toString(listviewImage[i]));
                        aList.add(hm);
                    }

                    //String[] from = {"listview_image", "listview_title", "listview_discription"};
                    String[] from = {"listview_image", "listview_title"};
                    int[] to = {R.id.listview_image, R.id.listview_item_title};


                    SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.contactlistview, from, to);
                    ListView androidListView = (ListView) findViewById(R.id.list_view);
                    androidListView.setAdapter(simpleAdapter);

                    //set pesan muncul ketika klik
                    /** androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String clicked = ((TextView) view.findViewById(R.id.listview_item_title)).getText().toString();
                    Toast.makeText(contactlist.this, clicked, Toast.LENGTH_SHORT).show();
                    }
                    });
                     **/

                    //Click dan Buka History
                    androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String clicked = ((TextView) view.findViewById(R.id.listview_item_title)).getText().toString();
                            Intent i = null;
                            i = new Intent(contactlist.this, contacttasklist.class);

                            Bundle b = new Bundle();
                            b.putString("friendemailaddressfriend", clicked);
                            b.putString("friendemailaddress", xUsername);
                            i.putExtras(b);

                            startActivity(i);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<friend>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void getFriendList3() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<friend>> call = api.getFriendList3(xUsername, xUsername);


        call.enqueue(new Callback<List<friend>>() {
            @Override
            public void onResponse(Call<List<friend>> call, retrofit2.Response<List<friend>> response) {
                List<friend> FriendList = response.body();

                String[] Friend_EmailAddress = new String[FriendList.size()];
                String[] Friend_EmailAddressFriend = new String[FriendList.size()];
                arrusername = new String[FriendList.size()];

                if (FriendList.size() > 0) {

                    //Lakukan Refresh Grid ya
                    // Array of strings for ListView Title
                    String[] listviewTitle = new String[FriendList.size()];
                    int[] listviewImage = new int[FriendList.size()];
                    String[] listviewShortDescription = new String[FriendList.size()];

                    for(int i = 0; i < FriendList.size(); i++) {
                        Log.e("contactlist", ""+xUsername+" VS " + FriendList.get(i).getemailaddress());
                        if (FriendList.get(i).getemailaddress().equals(xUsername)) {
                            listviewTitle[i] = FriendList.get(i).getemailaddressfriend();
                        }else {
                            listviewTitle[i] = FriendList.get(i).getemailaddress();
                        }
                        //sementara tetap tambah dengan contact photo dulu
                        listviewImage[i] = R.drawable.contacts;
                        //listviewShortDescription[i] = "";
                    }

                    arrusername = listviewTitle;

                    List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

                    for (int i = 0; i < FriendList.size() ; i++) {
                        HashMap<String, String> hm = new HashMap<String, String>();
                        hm.put("listview_title", listviewTitle[i]);
                        //hm.put("listview_discription", listviewShortDescription[i]);
                        hm.put("listview_image", Integer.toString(listviewImage[i]));
                        aList.add(hm);
                    }

                    //String[] from = {"listview_image", "listview_title", "listview_discription"};
                    String[] from = {"listview_image", "listview_title"};
                    int[] to = {R.id.listview_image, R.id.listview_item_title};


                    SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList, R.layout.contactlistview, from, to);
                    ListView androidListView = (ListView) findViewById(R.id.list_view);
                    androidListView.setAdapter(simpleAdapter);

                    //set pesan muncul ketika klik
                    /** androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String clicked = ((TextView) view.findViewById(R.id.listview_item_title)).getText().toString();
                    Toast.makeText(contactlist.this, clicked, Toast.LENGTH_SHORT).show();
                    }
                    });
                     **/

                    //Click dan Buka History
                    androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String clicked = ((TextView) view.findViewById(R.id.listview_item_title)).getText().toString();
                            Intent i = null;
                            i = new Intent(contactlist.this, contacttasklist.class);

                            Bundle b = new Bundle();
                            b.putString("friendemailaddressfriend", clicked);
                            b.putString("friendemailaddress", xUsername);
                            i.putExtras(b);

                            startActivity(i);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<friend>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }
}