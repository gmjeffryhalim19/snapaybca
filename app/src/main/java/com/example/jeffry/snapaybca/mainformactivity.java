package com.example.jeffry.snapaybca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class mainformactivity extends AppCompatActivity {

    static String CustomerName, UserName;
static Double Temp_Amount_Recipient = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainform);

        //Sembunyikan Action Bar
        getSupportActionBar().hide();

        //Ambil data dari Activity sebelumnya
        final Intent intent = getIntent();

        CustomerName = intent.getStringExtra("customername");
        UserName = intent.getStringExtra("username");
        Double BalanceInfo = intent.getDoubleExtra("balanceinfo",0.1);

        TextView TVMainBalanceInfo = (TextView) findViewById(R.id.TVMainBalanceInfo);
        TextView TVUsername = (TextView) findViewById(R.id.TVUsername);
        TVUsername.setText(CustomerName);

        //Update ke Database


        //Toast.makeText(getApplicationContext(), BalanceInfo.toString(), Toast.LENGTH_LONG).show();
        DecimalFormat formatter = new DecimalFormat("#,###");
        String strBalanceInfo = formatter.format(BalanceInfo);
        TVMainBalanceInfo.setText(strBalanceInfo);



        //Jika klik Photo ataupun Nama Profile maka menuju update profile
        ImageView ProfilePicture = (ImageView) findViewById(R.id.IVProfilePicture);
        ProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessProfileUpdate();
            }
        });

        TextView TVPProfileName = (TextView) findViewById(R.id.TVUsername);
        TVPProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessProfileUpdate();
            }
        });

        //Jika klik Photo Saldo ataupun Jumlah Saldo maka
        ImageView IVMainBalanceInfo = (ImageView) findViewById(R.id.IVMainBalanceInfo);
        IVMainBalanceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessBalanceActivity();
            }
        });

        TVMainBalanceInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessBalanceActivity();
            }
        });


        ImageView IVPayRequestPay = (ImageView) findViewById(R.id.IVPayRequestPay);
        IVPayRequestPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessPayRequestPay();
            }
        });
    }



    void ProcessProfileUpdate(){
        Intent i = null;
        i = new Intent(mainformactivity.this, ProfileActivity.class);
        i.putExtra("username", UserName);
        startActivity(i);
    }

    void ProcessBalanceActivity(){
        Intent i = null;
        i = new Intent(mainformactivity.this, BalanceActivity.class);
        i.putExtra("customername", CustomerName);
        i.putExtra("username", UserName);
        startActivity(i);
    }

    void ProcessPayRequestPay(){
        Intent i = null;
        i = new Intent(mainformactivity.this, contactlist.class);
        i.putExtra("customername", CustomerName);
        i.putExtra("username", UserName);
        startActivity(i);
    }

    void ProcessTopUp(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);
        //Cek dulu ada gak saldonya
        Call<List<Nama>> call = api.getLogin2(UserName);
        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                Double Temp_Amount_Recipient = 0.0;
                if(CustomerList.size() > 0) {
                    Temp_Amount_Recipient = CustomerList.get(0).getbalanceinfo();
                    //Log.e("taskpay","RCP 3" + Temp_Amount_Recipient);
                }
                //Jika berhasil maka update data ke recipient
            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Log.e("taskpay", t.toString());
            }
        });


        //Update Balance Info
        Retrofit UBretrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        Api UBapi = UBretrofit.create(Api.class);
        Call<Nama> UBcall = UBapi.setUpdateBalance(UserName, (Temp_Amount_Recipient+100000));
        UBcall.enqueue(new Callback<Nama>() {
            @Override
            public void onResponse(Call<Nama> call, Response<Nama> response) {

                Toast.makeText(mainformactivity.this, "User has been top up Rp. 100.000,-", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Nama> call, Throwable t) {
                Log.e("taskpay", t.toString());
            }
        });
    }
}
