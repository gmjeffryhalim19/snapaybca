package com.example.jeffry.snapaybca;

public class transactioninquiry {
	private Double requestamount;
	private Double amount;
	private String transactiondate;
	private String notes;
	private String accountstatementid;
	private String paymentpicture;
	private String statuspayment;
	private String emailaddress;
	private String requestfrom;
	private String transactiontype;
	private String transactionid;
	private Double currentbalance;
	private String emailaddresssender;
	private String lastaccountstatementid;
	private String emailaddressrecipient;
	private String id;
	private String currencyid;
	private String ischannel;

	public void setRequestamount(Double requestamount){
		this.requestamount = requestamount;
	}

	public Double getRequestamount(){
		return requestamount;
	}

	public void setAmount(Double amount){
		this.amount = amount;
	}

	public Double getAmount(){
		return amount;
	}

	public void setTransactiondate(String transactiondate){
		this.transactiondate = transactiondate;
	}

	public String getTransactiondate(){
		return transactiondate;
	}

	public void setNotes(String notes){
		this.notes = notes;
	}

	public String getNotes(){
		return notes;
	}

	public void setAccountstatementid(String accountstatementid){
		this.accountstatementid = accountstatementid;
	}

	public String getAccountstatementid(){
		return accountstatementid;
	}

	public void setPaymentpicture(String paymentpicture){
		this.paymentpicture = paymentpicture;
	}

	public String getPaymentpicture(){
		return paymentpicture;
	}

	public void setStatuspayment(String statuspayment){
		this.statuspayment = statuspayment;
	}

	public String getStatuspayment(){
		return statuspayment;
	}

	public void setEmailaddress(String emailaddress){
		this.emailaddress = emailaddress;
	}

	public String getEmailaddress(){
		return emailaddress;
	}

	public void setRequestfrom(String requestfrom){
		this.requestfrom = requestfrom;
	}

	public String getRequestfrom(){
		return requestfrom;
	}

	public void setTransactiontype(String transactiontype){
		this.transactiontype = transactiontype;
	}

	public String getTransactiontype(){
		return transactiontype;
	}

	public void setTransactionid(String transactionid){
		this.transactionid = transactionid;
	}

	public String getTransactionid(){
		return transactionid;
	}

	public void setCurrentbalance(Double currentbalance){
		this.currentbalance = currentbalance;
	}

	public Double getCurrentbalance(){
		return currentbalance;
	}

	public void setEmailaddresssender(String emailaddresssender){
		this.emailaddresssender = emailaddresssender;
	}

	public String getEmailaddresssender(){
		return emailaddresssender;
	}

	public void setLastaccountstatementid(String lastaccountstatementid){
		this.lastaccountstatementid = lastaccountstatementid;
	}

	public String getLastaccountstatementid(){
		return lastaccountstatementid;
	}

	public void setEmailaddressrecipient(String emailaddressrecipient){
		this.emailaddressrecipient = emailaddressrecipient;
	}

	public String getEmailaddressrecipient(){
		return emailaddressrecipient;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCurrencyid(String currencyid){
		this.currencyid = currencyid;
	}

	public String getCurrencyid(){
		return currencyid;
	}

	public void setIschannel(String ischannel){
		this.ischannel = ischannel;
	}

	public String getIschannel(){
		return ischannel;
	}


	@Override
 	public String toString(){
		return 
			"transactioninquiry{" +
			"requestamount = '" + requestamount + '\'' + 
			",amount = '" + amount + '\'' + 
			",transactiondate = '" + transactiondate + '\'' + 
			",notes = '" + notes + '\'' + 
			",accountstatementid = '" + accountstatementid + '\'' + 
			",paymentpicture = '" + paymentpicture + '\'' + 
			",statuspayment = '" + statuspayment + '\'' + 
			",emailaddress = '" + emailaddress + '\'' + 
			",requestfrom = '" + requestfrom + '\'' + 
			",transactiontype = '" + transactiontype + '\'' + 
			",transactionid = '" + transactionid + '\'' + 
			",currentbalance = '" + currentbalance + '\'' + 
			",emailaddresssender = '" + emailaddresssender + '\'' + 
			",lastaccountstatementid = '" + lastaccountstatementid + '\'' + 
			",emailaddressrecipient = '" + emailaddressrecipient + '\'' + 
			",id = '" + id + '\'' + 
			",currencyid = '" + currencyid + '\'' +
			",ischannel = '" + ischannel + '\'' +
			"}";
		}
}
