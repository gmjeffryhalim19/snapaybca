package com.example.jeffry.snapaybca;

/**
 * Created by Jeffry on 13/11/2017.
 */

//import com.google.gson.annotations.SerializedName;
public class Nama{

    private String dateofbirth;
    private String customernumber;
    private String password;
    private String ktpno;
    private String companycode;
    private String nama;
    private String emailaddress;
    private String id;
    private String customername;
    private String nomor;
    private String accountno;
    private Double balanceinfo;
    private String profilepicturelocation;

    public void setdateofbirth(String dateofbirth){
        this.dateofbirth = dateofbirth;
    }

    public String getdateofbirth(){
        return dateofbirth;
    }

    public void setcustomernumber(String customernumber){
        this.customernumber = customernumber;
    }

    public String getcustomernumber(){
        return customernumber;
    }

    public void setpassword(String password){
        this.password = password;
    }

    public String getpassword(){
        return password;
    }

    public void setktpno(String ktpno){
        this.ktpno = ktpno;
    }

    public String getktpno(){
        return ktpno;
    }

    public void setcompanycode(String companycode){
        this.companycode = companycode;
    }

    public String getcompanycode(){
        return companycode;
    }

    public void setnama(String nama){
        this.nama = nama;
    }

    public String getnama(){
        return nama;
    }

    public void setemailaddress(String emailaddress){
        this.emailaddress = emailaddress;
    }

    public String getemailaddress(){
        return emailaddress;
    }

    public void setid(String id){
        this.id = id;
    }

    public String getid(){
        return id;
    }

    public void setcustomername(String customername){
        this.customername = customername;
    }

    public String getcustomername(){
        return customername;
    }

    public void setnomor(String nomor){
        this.nomor = nomor;
    }

    public String getnomor(){
        return nomor;
    }

    public void setaccountno(String accountno){
        this.accountno = accountno;
    }

    public String getaccountno(){
        return accountno;
    }

    public void setBalanceinfo(double balanceinfo){
        this.balanceinfo = balanceinfo;
    }

    public Double getbalanceinfo(){
        return balanceinfo;
    }

    public void setprofilepicturelocation(String profilepicturelocation){
        this.profilepicturelocation = profilepicturelocation;
    }

    public String getprofilepicturelocation(){
        return profilepicturelocation;
    }

    @Override
    public String toString(){
        return
                "transaction{" +
                        "dateofbirth = '" + dateofbirth + '\'' +
                        ",customernumber = '" + customernumber + '\'' +
                        ",password = '" + password + '\'' +
                        ",ktpno = '" + ktpno + '\'' +
                        ",companycode = '" + companycode + '\'' +
                        ",nama = '" + nama + '\'' +
                        ",emailaddress = '" + emailaddress + '\'' +
                        ",id = '" + id + '\'' +
                        ",customername = '" + customername + '\'' +
                        ",nomor = '" + nomor + '\'' +
                        ",accountno = '" + accountno + '\'' +
                        ",balanceinfo = '" + balanceinfo + '\'' +
                        ",profilepicturelocation = '" + profilepicturelocation + '\'' +
                        "}";
    }
}