package com.example.jeffry.snapaybca;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.net.URL;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileActivity extends AppCompatActivity {
    TextView tvProfilePhoto;
    static String xUsername;
    static String ProfilePhotoURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //Set Logo, set Back
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.logo_snapaybca_actionbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");


        Intent intent = getIntent();;
        xUsername = intent.getStringExtra("username");

        //getLogin2();

        //tvProfilePhoto = (TextView) findViewById(R.id.tvProfilePhoto);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();
        Api api = retrofit.create(Api.class);
        Call<List<Nama>> call = api.getLogin2(xUsername);

        Log.e("ProfileActivity",xUsername);

        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                EditText ETAccountNo = (EditText) findViewById(R.id.ETAccountNo);
                EditText ETPPFullName = (EditText) findViewById(R.id.etPPFullName);
                //TextView tvProfilePhoto = (TextView) findViewById(R.id.tvProfilePhoto);

                //Creating an String array for the ListView
                String[] Username = new String[CustomerList.size()];
                String[] AccountNo = new String[CustomerList.size()];
                String[] FullName = new String[CustomerList.size()];
                String[] ProfilePictureLocation= new String[CustomerList.size()];

                final String URLFoto;

                //looping through all the heroes and inserting the names inside the string array
                for (int i = 0; i < CustomerList.size(); i++) {
                    Username[i] =  CustomerList.get(i).getemailaddress();
                    AccountNo[i] = CustomerList.get(i).getaccountno();
                    FullName[i] = CustomerList.get(i).getnama();
                    ProfilePictureLocation[i] = CustomerList.get(i).getprofilepicturelocation();

                    if (Username[i].equals(xUsername)) {
                        ProfilePhotoURL = ProfilePictureLocation[i];
                        //Log.e("ProfileActivity",ProfilePhotoURL);
                        //tvProfilePhoto.setText(ProfilePictureLocation[i]);
                        ETPPFullName.setText(FullName[i]);
                        ETAccountNo.setText(AccountNo[i]);


                        Log.e("ProfileActivity", ProfilePictureLocation[0]);
                        ImageView IVProfilePhoto = (ImageView) findViewById(R.id.IVProfilePhoto);
                        Picasso.with(ProfileActivity.this).load(ProfilePictureLocation[0]).into(IVProfilePhoto);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //ProfilePhotoURL = tvProfilePhoto.getText().toString();
        //Log.e("ProfileActivity", ProfilePhotoURL);
        /**
        if (ProfilePhotoURL.toString().trim().length() > 0) {
            Picasso.with(this)
                    .load(ProfilePhotoURL)
                    .into(IVProfilePhoto);
        }
        **/



        Button btnUpdateProfile = (Button) findViewById(R.id.btnUpdateProfile);
        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProcessUpdateProfile();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void ProcessUpdateProfile(){
        String AccountNo;

        EditText ETAccountNo = (EditText) findViewById(R.id.ETAccountNo);
        AccountNo = ETAccountNo.getText().toString();

        //Update ke Database

        /**
        Picasso.with(this)
                .load("https://vignette.wikia.nocookie.net/lyricwiki/images/9/99/Dua_Lipa_-_Dua_Lipa.png/revision/latest?cb=20170525190129")
                .into(IVProfilePhoto);
         **/
    }

    private void getLogin2() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<Nama>> call = api.getLogin2(xUsername);

        Log.e("ProfileActivity",xUsername);

        call.enqueue(new Callback<List<Nama>>() {
            @Override
            public void onResponse(Call<List<Nama>> call, Response<List<Nama>> response) {
                List<Nama> CustomerList = response.body();

                Log.e("ProfileActivity",xUsername);

                EditText ETPPFullName = (EditText) findViewById(R.id.etPPFullName);
                EditText ETAccountNo = (EditText) findViewById(R.id.ETAccountNo);
                //TextView tvProfilePhoto = (TextView) findViewById(R.id.tvProfilePhoto);

                //Creating an String array for the ListView
                String[] Username = new String[CustomerList.size()];
                String[] AccountNo = new String[CustomerList.size()];
                String[] ProfilePictureLocation= new String[CustomerList.size()];

                //looping through all the heroes and inserting the names inside the string array
                for (int i = 0; i < CustomerList.size(); i++) {
                    Username[i] =  CustomerList.get(i).getemailaddress();
                    AccountNo[i] = CustomerList.get(i).getaccountno();
                    ProfilePictureLocation[i] = CustomerList.get(i).getprofilepicturelocation();
                    ETPPFullName.setText(CustomerList.get(i).getnama());

                    if (Username[i].equals(xUsername)) {
                        ProfilePhotoURL = ProfilePictureLocation[i];

                        tvProfilePhoto.setText(ProfilePictureLocation[i]);
                        ETAccountNo.setText( AccountNo[i] );
                        //break;
                    }
                }

                //displaying the string array into listview
                //listView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, heroes));

            }

            @Override
            public void onFailure(Call<List<Nama>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
